const containerData = document.getElementById('cont-table'),
      btn = document.getElementById('btn-table'),
      btnNews = document.getElementById('btn-news'),
      loader = document.getElementById('loader'),
      loaderTwo = document.getElementById('loader2'),
      contNoticias = document.getElementById('noticias-cont');
      // --
loader.style.display = 'none';
loaderTwo.style.display = 'none';
// Scripts AJAX 
btn.addEventListener('click', event => {
    loader.style.display = 'block';
    // --
    const xhr = new XMLHttpRequest();

    // Indicando método y ruta de busqueda | true = Es asincrono
    xhr.open('GET','data.html',true);

    // Tareas una vez traida la data
    xhr.addEventListener('load', obj => {
        console.log(obj.target);
        // Retardando para ver animación
        setTimeout(() => {
            loader.style.display = 'none';
            containerData.innerHTML = obj.target.responseText;
        },2000)
    } )

    // Empezando solicitud
    xhr.send();
})
// --
btnNews.addEventListener('click', e => {
    loaderTwo.style.display = 'block';
    const XHR = new XMLHttpRequest();
    // --
    XHR.open('GET','news.txt',true);
    // -
    XHR.addEventListener('load', obj => {
        // console.log(obj.target.responseText);
        setTimeout(() => {
            const data = JSON.parse(obj.target.responseText); // Pasando a JSON
            loaderTwo.style.display = 'none';
            drawJSON(data);
        },3000)

    })
    // --
    XHR.send();
})
// --
function drawJSON (json){
    // --
    contNoticias.innerHTML = '';
    const temp = document.createDocumentFragment();
    // --
    json.forEach(nw => {
        let containerNew = document.createElement('div'),
            title = document.createElement('h4'),
            content = document.createElement('p'),
            autor = document.createElement('span');
        // Creando elementos
        title.textContent = nw.Titulo;
        content.textContent = nw.Texto;
        autor.textContent = nw.Autor;
        // Estilos
        containerNew.classList.add('container','card-panel','w-100');
        title.classList.add('text-center','text-uppercase');
        content.classList.add();  
        autor.classList.add('form-text','text-center');     
        // Agragando elementos a contenedor de notica
        containerNew.appendChild(title);
        containerNew.appendChild(content);
        containerNew.appendChild(autor);
        // Agregando a HTML
        temp.appendChild(containerNew);
    });
    contNoticias.appendChild(temp);
}