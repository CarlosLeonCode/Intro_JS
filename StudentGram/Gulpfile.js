const gulp = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
// --
gulp.task('styles', () =>{
  gulp.src('index.scss')
      .pipe(sass())
      .pipe(rename('main.css'))
      .pipe(gulp.dest('public/css'));
  }
)
