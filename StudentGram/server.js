var express = require('express');
var app = express();
// --
app.set('view engine','pug')
app.get('/',function(req,res){
  res.render('index')
})
// --
app.listen(3000,function(err){
  // -- Si error es diferente de null
  if (err) return console.log('Hubo un error'), process.exit(1);
  // -- De lo contrario
  console.log('Escuchando mensaje por el puerto 3000');
})
