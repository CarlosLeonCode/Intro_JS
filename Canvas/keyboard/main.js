//---------------------------------------------CON TECLADO
document.addEventListener("keydown",drawKeyb);
var teclas = {
  UP: 38,
  DOWN: 40,
  LEFT: 37,
  RIGHT: 39
};
console.log(teclas);

var cuadrito = document.getElementById('canvas');
var papel = cuadrito.getContext("2d");
var x = 150;
var y = 150;
dibujarLinea("black",x-1,y-1,x+1,y+1,papel);
//Funcion para crear linea
function dibujarLinea(colors, xinicial, yinicial, xfinal, yfinal,lienzo)
{
  lienzo.beginPath();
  lienzo.strokeStyle = colors;
  lienzo.lineWidth = 3;
  lienzo.moveTo(xinicial, yinicial);
  lienzo.lineTo(xfinal, yfinal);
  lienzo.stroke();
  lienzo.closePath();
}
//Crear linea con teclado
function drawKeyb(evento){
  var colorcito = "purple";
  var movimiento = 10;
//----------------------------------------------------------------------------
  switch (evento.keyCode) {
    case teclas.UP:
        dibujarLinea(colorcito , x , y , x , y - movimiento , papel);
        y = y - movimiento;
      break;
    case teclas.DOWN:
    dibujarLinea(colorcito , x , y , x , y + movimiento , papel);
    y = y + movimiento;
    break;
    case teclas.LEFT:
    dibujarLinea(colorcito , x , y , x - movimiento , y , papel);
    x = x - movimiento;
    break;
    case teclas.RIGHT:
    dibujarLinea(colorcito , x , y , x + movimiento , y , papel);
    x = x + movimiento;
    break;

    default:
      console.log('Otra tecla');
    break;
  }
}

//-------------------------------CON MOUSE
