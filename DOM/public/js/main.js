// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>SELECCIONANDO Y BASES

// Seleccionando nodos mediante id
const title = document.getElementById('title');
// title.style.background = 'lightblue';
// Seleccionando nodos mediante selecciones css
// document.querySelector() - element.querySelector()
// document.querySelectorAll() - element.querySelectorAll()
const titleTwo = document.querySelectorAll('header div h1');
var elementOne = document.querySelector('section div button');
// console.log(elementOne);
// -------------------------------------------------------------

                          // ATRIBUTOS
// Array.from  = Me permite convertir en array.
// getAttribute
// setAttribute ('atributo','valor')
// removeAttribute('atributo')
// classList{
//   .add()
//   .remove()
//   .toggle()
// }
// id
// value

function changeColor(){
  let element = document.getElementById('btn1');
  if(element){
    element.classList.toggle('btn-danger');
  }
}

function addColor(){
  let element = document.getElementById('btn2');
  if(element){
    element.textContent = "Con color";
    element.classList.add('btn-success');
  }
}

function removeColor(){
  let element = document.querySelector('body section .botones #btn3');
  if(element){
    element.textContent = "Sin color";
    element.classList.remove('text-light','btn-dark');
  }
}

                          // PROPIEDES DE CONTENIDO

// innerHTML  -- Me trae o inserta hrml
// textContent -- Me trae o inserta texto
// outerHTML -- Me trae elementos como string
// alt + 96 = backtrick me permite escribir código html mejor - ``

function putCard(){
  if(document.querySelector('body section .crd')){
    let element = document.querySelector('body section .crd');
    element.innerHTML = `
    <div class="card mt-5 text-center">
    <div class="card-header"><h1 class="text-uppercase">Utilizando propiedades de contenido</h1></div>
    <div class="card-body"><p>Creando elemento con innerHTML</p></div>
    </div>`;
    // -- Cambio titulo del botón
    let boton = document.getElementById('btn4');
    boton.textContent = 'Card creada';
    // -- Mostrando elemento en consola
    console.log(element.outerHTML);
  }
}

                          // CREANDO ELEMENTOS

// document.createElement()
// element.appendChild() -- Añade elemento al final de otro elemento

function addCard(){
  if(document.querySelector('body section .crd2')){
    let elementPhater = document.querySelector('body section .crd2');
    let content = document.createElement('div');
    content.classList.add('card', 'mt-5', 'text-center','rounded');
    content.innerHTML = `<div class="card-body bg-dark text-light">Elemento añadido</div>`;
    // --
    console.log(content.outerHTML);
    // --Agregando elemento a div crd2
    elementPhater.appendChild(content);
  }
}

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>EVENTOS MOUSE

// -------|addEventListener
// element.addEventListener('evento','funcion a ejecutar')
// --
if(document.getElementById('btn-event1')){
  let botonOne = document.getElementById('btn-event1');
  // --
  let funcion = evnt => {
    alert('Esto es un evento click /  ' + evnt.target.textContent);
  }
  // target me trae el elemento donde se género el evento
  botonOne.addEventListener('click',e =>{
    funcion(e);
  })
}

                          // E.DE MOUSE
// click - Al dar Click
// dbclick - Doble Click
// mouseenter - Mientras el cursor está en un área
// mouseleave - Cuando el cursor sale de un área
// contextmenu - click derecho
// mouseup - Al soltar el click
// mousedown - Al hacer click sostenido
// mousemove - Cuando el cursor se mueve en un elemento.

// -----
if(document.getElementById('btn-event2')){
  let boton = document.getElementById('btn-event2');
  // --
  let funcion = e => {
    alert('Esto es un evento doble click / ' + e.target.textContent);
  }
  boton.addEventListener('dblclick',e => {
    funcion(e);
  });
}
// -----
if(document.getElementById('areaOne')){
  let area = document.getElementById('areaOne');
  // --
  let funcionOne = () => {
      area.classList.remove('bg-dark','text-light');
      area.classList.add('bg-info','text-dark');
      area.style.transition = "all .2s";
  }
  // --
  // --
  let funcionTwo = () => {
      area.classList.remove('bg-info','text-dark');
      area.classList.add('bg-dark','text-light');
      area.style.transition = "all .2s";
  }
  // --
  area.addEventListener('mouseenter', () =>{
    funcionOne();
  });
  // --
  area.addEventListener('mouseleave', () =>{
    funcionTwo();
  })
}
// -----
if(document.getElementById('evntUp')){
  // --
  let boton = document.getElementById('evntUp');
  // --
  let funcion = () => {
    alert('Has soltado el click');
    boton.classList.toggle('btn-dark');
  }
  // --
  boton.addEventListener('mouseup', () => {
    funcion();
  })
}
// ---
if(document.getElementById('evntDown')){
  let boton = document.getElementById('evntDown');
  // --
  let funcion = () => {
    alert('Has presionado el click');
    boton.classList.toggle('btn-warning');
  }
  // --
  boton.addEventListener('mousedown', () => {
    funcion();
  })
}
// ----m.move

if(document.getElementById('areaTwo')){
  let area = document.getElementById('areaTwo');
  // --
  let funcion = evnt =>{
    area.style.background = `#${evnt.pageY}`;
    area.style.transition = 'all .2s';
  }
  // --
  area.addEventListener('mousemove', infoE => {
    funcion(infoE);
  });
}

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>EVENTOS TECLADO

// Keydown - Al precionar tecla
// Keyup - Dejar de presionar tecla
// Keypress
// --
if(document.getElementById('game')){
  var x = 0,y = 0;
  let place = document.getElementById('game');
  let personaje = document.querySelector('#game .img-character');
  // ---
  let move = direction => {
    switch (direction) {
      case 'arriba':
        y--;
        break;
      case 'abajo':
        y++;
        break;
      case 'derecha':
        x++;
        break;
      case 'izquierda':
        x--;
        break;
      case 'saltar':
        y--;
        setTimeout(function () {
          y++;
          personaje.style.transform = `translate(${x*80}px,${y*80}px)`;
          personaje.style.transition = 'all .3s';
        }, 380);
        break;
    }
    personaje.style.transform = `translate(${x*80}px,${y*80}px)`;
    personaje.style.transition = 'all .3s';
  }
  // --
  let changeInvert = () => {
    place.style.filter = "invert(100%)";
    place.style.transition = "all .3s";
    setTimeout(function () {
      place.style.filter = "invert(1%)";
      place.style.transition = "all .3s";
    }, 200);
  }

  addEventListener('keydown', info => {
    // --
    switch (info.keyCode) {
      case 38,87:
        move('arriba');
        break;
      case 40,83:
        move('abajo');
        break;
      case 39,68:
        move('derecha');
        break;
      case 37,65:
        move('izquierda');
        break;
      case 32:
        move('saltar');
        break;
      case 81:
        changeInvert();
        break
    }
    console.log(info.keyCode)
  });
}
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>DE FORMULARIO
// submit - Al enviar un formulario
// change - Al seleccionar por ejemplo un checkbox
// focus - Al estar seleccionado en un input
// blur - Al salir o dejar de  seleccionar un input
// reset
// DOMContentLoaded -- Me permite ejecutar un script despues de que el DOM se cargue

addEventListener('DOMContentLoaded', () => {
  // --
if(document.getElementById('terminos')){
  let boton = document.querySelector('body section .card-body form button');
  boton.setAttribute('disabled','disabled');
  // --
  let habilitar = estado => {
    if(estado === true){
      // console.log('Habilitado');
      boton.removeAttribute('disabled');
    }else{
      // console.log('Inhabilitado');
      boton.setAttribute('disabled','disabled');
    }
  }
  // --
  let checkbox = document.getElementById('terminos');
  // --
  checkbox.addEventListener('change', e => {
    // console.log(e);
    habilitar(e.target.checked);
  });
}
// ---
  if(document.getElementById('formEvent')){
    let inputOne = document.getElementById('inpOne');
    inputOne.addEventListener('focus', e => {
      console.log('Focus inside');
    });
    // --
    inputOne.addEventListener('blur', e => {
      console.log('Focus outside');
    });
  }
})
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>EVENTOS DEL DOM
// Scroll - Cuando se hace escroll en el navegador
// Resize - Cuando se redimenciona la página
addEventListener('DOMContentLoaded', () => {
      // ---
      addEventListener('scroll', e => {
        // console.log(e.pageY);
      });

})
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>EVENTOS MULTIMEDIA
// play
// pause
if(document.querySelector('body .container .reproductor')){
  let cancion = document.getElementById('song'),
      botonPlay = document.querySelectorAll('body .container .reproductor button')[0],
      botonPause = document.querySelectorAll('body .container .reproductor button')[1],
      text1 = document.getElementById('textoOne'),
      contenedor = document.getElementById('reproductor')
  console.log(cancion.volume);
  // --
  botonPlay.addEventListener('click', () => {
    cancion.play();
  })
  // --
  botonPause.addEventListener('click', () =>{
    cancion.pause();
  });
  // --
  cancion.addEventListener('play', () =>{
    text1.innerHTML = "En reproducción";
  })
  // --
    cancion.addEventListener('pause', () =>{
      text1.innerHTML = "En pausa";
    })
  // -----------------------------------
  // addEventListener('load',init,false)
  //   function init(){
  //   try{
  //     var context = null;
  //     window.AudioContext = window.AudioContext||window.webkitAudioContext;
  //     context = new AudioContext();
  //     console.log('Api exitosa');
  //     // ---
  //     let soundBuffer = null;
  //     // --
  //     window.AudioContext = window.AudioContext||window.webkitAudioContext;
  //     let context = new AudioContext();
  //     // --
  //     // cargando sonido
  //     let loadSound = ulr =>{
  //       let request = new XMLHttpRequest();
  //       request.open('GET',url,true);
  //       request.responseType = 'arraybuffer';
  //       // --
  //       // decodificando asin
  //       request.onload = function () {
  //         context.decodeAudioData(request.response,function(buffer){
  //           soundBuffer = buffer;
  //         }, onError);
  //       }
  //       request.send();
  //     }
  //   }catch(e){
  //     console.log('error api');
  //   }
  // }
}
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Delegación
if(document.querySelector('body .galeria .imagenes')){
  // --
  let containerDad = document.getElementById('Daddy');
  // --
  var sad = ["",""];
  var c =0;
  containerDad.addEventListener('click', e =>{
    // --
    let elementSeleccionado = e.target,
        imagenes = Array.from(containerDad.querySelectorAll('img')),
        i = imagenes.indexOf(elementSeleccionado);
        let imagenTemp = e.target;
        // var sel1=e.target;
        // var sel2=e.target;
        sad[c]=e.target.id;
        //console.log(`imagen ${i + 1}`);
        // --
              console.log(sad);
        c=c+1;
        if (c===1) {
          c=0;
        }
        imagenTemp.setAttribute('src','public/img/Back-8.png');
        if (sad[0] == sad[1]) {
      alert("IGUALES MI PERRITO");
     }
  });

   containerDad.addEventListener('dblclick', e =>{
    // --
    let elementSeleccionado = e.target,
        imagenes = Array.from(containerDad.querySelectorAll('img')),
        i = imagenes.indexOf(elementSeleccionado);
        let imagenTemp = e.target;
        //console.log(`imagen ${i + 1}`);
        // --
        imagenTemp.setAttribute('src','public/img/Back-2.png');
  });
}


// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>DOM Recorrer y manipular

// DOM traversing - atravesar o desplazarse

// >>Hermanos
//  -nextSibling
//  -nextElementSibling
//  -previousSibling
//  -previousElementSibling

// >>Padre
//   -parentNode
//   -parentElement

// >>Hijo
//   -childNodes
//   -children
//   -firstChild
//   -firstElementChild
//   -lastChild
//   -lastElementChild
//   -hasChildNodes() - True or False - Me permite saber si un elemento tiene elementos hijos

if(document.getElementById('mani-recorrDOM')){
  // --
  let padre = document.getElementById('dad');
  console.log(padre.firstElementChild); //Primer elemento del padre
  // --
  let child = document.querySelectorAll('#mani-recorrDOM #dad div')[5];
  console.log(child.previousElementSibling); //Hermano anterior de child
  console.log(child.previousSibling); //Nodo anterior de child
}
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Agregar elementos
// -InsertBefore(nuevoElemento,elementoAncla)
if(document.getElementById('insertElements')){
  let ancla = document.querySelectorAll('#insertElements div')[2],
      padre = document.getElementById('insertElements');
      newElement = document.createElement('div');
      newElement.classList.add('px5','py5','bd-light','d-inline-block','rounded');
      newElement.textContent = 'Elemento nuevo';
      // --
      padre.insertBefore(newElement,ancla);
}
