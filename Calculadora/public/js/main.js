/* Algoritmo
->1 => Selecciona número.
        ->Puede seleccionar varios.
        ->Puede borrar.
->2 =>  Selecciona operador
        ->Sumar
        ->Restar
        ->Dividir
        ->Multiplicar
->3 => ¿termino? ? => 4 : => 1
->4 => igualar
*/
if(document.querySelector('body')&&(document.querySelector('body .container div div .card .card-body .options-calc .container .row .cont-buttons'))){
    // -- Obteniendo valores de cada botón 0-9
    const options = document.querySelector('body .container .row div .mycard .card-body .options-calc div .row .cont-buttons'),
        zero = options.firstElementChild,
        one = zero.nextElementSibling,
        two = one.nextElementSibling,
        three = two.nextElementSibling,
        four = three.nextElementSibling,
        five = four.nextElementSibling,
        six = five.nextElementSibling,
        seven = six.nextElementSibling,
        eight = seven.nextElementSibling,
        nine = eight.nextElementSibling,
    // -- Obteniendo opciones de calculadora (operadores)
        add = document.querySelector('body .container .row div .mycard .card-body .options-calc div .row .cont-options-calc').firstElementChild,
        substraction = add.nextElementSibling,
        multiply = substraction.nextElementSibling,
        divide = multiply.nextElementSibling,
        equals = divide.nextElementSibling,
    // -- Obteniendo displays
        display = document.getElementById('displayOne'),
        displayResult = document.getElementById('displayTwo'),
        clear = document.getElementById('btnClear');
    // --
    var firtsNumber = null,
        resultOperation = null,
        secondNumber = null,
        operator = null, 
        datosAOperar = "";                                          // Almacena los números a operar
    // --
    const addValue = value => {                                     // función para agregar datos al display y al nodesByDisplay(array)
        // --
        let arrayDisplay = Array.from(display.value);               // Obtengo datos del display
        // --
        if((arrayDisplay.length == 1)&&(arrayDisplay[0] == 0)&&(value != 0)){     // Si el display tiene un único valor y es igual a cero
            display.value = value;                                  // Vacio el display
        }else{                                                      
            if(value != '='){
                let stringDataDisplay = display.value;              // Creo una variable para crear un string con los datos en display.
                display.value = stringDataDisplay + value;          // Concateno el nuevo valor con el anterior, formando un número y lo muestro en display.
            }
        }   
        // ---
        if((value == '+')||(value == '-')||(value == '*')||(value == '/')||(value == '=')){ // Si se ingresa un operador, no se añade a datosAOperar ya que solo guarda números
            // --
            operator = value;
            // --
            if((firtsNumber == null)){                              // Si el primer número no existe
                firtsNumber = datosAOperar;                         // Asigno la cadena a la variable firtsNumbers
                console.log(`Mostrando firtNumber despues de asignar valor ${firtsNumber}`);
            }else{
                secondNumber = datosAOperar;                        // Asigno la cadena a la variable secondNumbers
                console.log(`Mostrando secondNumber despues de asignar valor ${firtsNumber}`);
            }
            // --- Llamo a la función para comenzar a operar.
            if((firtsNumber != null)&&(secondNumber != null)&&(operator != null)){
                operar(operator);
            }
            // --
        }else{                                                      // Si no es un operador.
            let tempDatos = datosAOperar;   
            datosAOperar = tempDatos + value;                       // Concateno los datos a la variable datosAOperar y creo un número nuevo.
            console.log(`Los datos a operar son: ${datosAOperar}`);
        }
        if(value == '='){
            displayResult.value = resultOperation;
        }
    }
    // --
    const operar = operador => {                                    // Función para operar
        try{
            let resultado;
            console.log(`${firtsNumber} -- ${secondNumber}`);
            switch (operador) {
                case '+':                                           // Sumar
                    resultado = parseInt(firtsNumber) + parseInt(secondNumber);
                    displayResult.tzextContent = resultado;
                    break;
                case '-':                                           // Restar
                    resultado = parseInt(firtsNumber) - parseInt(secondNumber);
                    displayResult.textContent = resultado;
                    break
                case '*':                                           // Multiplicar
                    resultado = parseInt(firtsNumber) * parseInt(secondNumber);
                    displayResult.textContent = resultado;
                    break
                case '/':                                           // Dividir
                    resultado = parseInt(firtsNumber) / parseInt(secondNumber);
                    displayResult.textContent = resultado;
                    break
                case '=':
                    displayResult.value = resultado;
                    break;
                default:
                    console.log('Error al operar');
                    break
            }
            resultOperation = resultado;
            datosAOperar = "";                                      // La dejo vacia para formar el nuevo número a operar.
            firtsNumber = resultado;                                // Asigno el resultado al primer número.
        }catch(error){
            console.error(error)
        }
    }

    // --
    const clearValues = () => {                               // Función para limpiar los valores de la calculadora.
        display.value = '0';                             
        displayResult.textContent = 'Resultado';
        datosAOperar = "";
        firtsNumber = null;
        secondNumber = null;

    }
    // --

    // ------- capturando eventos de número ------------------------------------------------

    if(document.getElementById('btnZero')){                  // Event.Cero
        zero.addEventListener('click', () =>{
            addValue(zero.textContent);
        });
    }
    if(document.getElementById('btnOne')){                   // Event.Uno
        one.addEventListener('click', () => {
            addValue(one.textContent);
        });
    }
    if(document.getElementById('btnTwo')){                   // Event.Dos
        two.addEventListener('click', () => {
            addValue(two.textContent);
        });
    }
    if(document.getElementById('btnThree')){                // Event.Tres
        three.addEventListener('click', () => {
            addValue(three.textContent);
        });
    }
    if(document.getElementById('btnFour')){                 // Event.Cuatro
        four.addEventListener('click', () => {
            addValue(four.textContent);
        });
    }
    if(document.getElementById('btnFive')){                 // Event.Cinco          
        five.addEventListener('click', () => {
            addValue(five.textContent);
        });
    }
    if(document.getElementById('btnSix')){                  // Event.Seis
        six.addEventListener('click', () => {
            addValue(six.textContent);
        });
    }
    if(document.getElementById('btnSeven')){                // Event.Siete                
        seven.addEventListener('click', () => {
            addValue(seven.textContent);
        });
    }
    if(document.getElementById('btnEight')){                // Event.Ocho                
        eight.addEventListener('click', () => {
            addValue(eight.textContent);
        });
    }
    if(document.getElementById('btnNine')){                 // Event.Nueve             
        nine.addEventListener('click', () => {
            addValue(nine.textContent);
        });
    }
    if(document.getElementById('btnClear')){                // Event.Clear             
        clear.addEventListener('click', () => {
            clearValues();
        });
    }

    // ------- Eventos de operador ------------------------------------------------

    var containerOperators = document.getElementById('operators'),              // Obteniendo contenedor de operadores
        operatorsChildren = containerOperators.querySelectorAll('button');      // Obteniendo operadores en NodeList
    // --
    if(operatorsChildren[0]){                               // Operador suma
        operatorsChildren[0].addEventListener('click', () => {
            addValue('+');
        });
    }
    // -
    if(operatorsChildren[1]){                               // Operador resta
        operatorsChildren[1].addEventListener('click', () => {
            addValue('-');
        });
    }   
    if(operatorsChildren[2]){                               // Operador multiplicación                    
        operatorsChildren[2].addEventListener('click', () => {
            addValue('*');
        });
    }       
    if(operatorsChildren[3]){                               // Operador división 
        operatorsChildren[3].addEventListener('click', () => {
            addValue('/');
        });
    }
    // --
    if(operatorsChildren[4]){                               // ---- Resultado
        operatorsChildren[4].addEventListener('click', () => {
        addValue('=');
        })
    }

    // ------- Eventos de teclado ------------------------------------------------

    function detectingkeyboard(numberKeyCode,valueKeyboard){
        // --
        switch (numberKeyCode){
            case 49:                                        // Número 1 top
            case 97:                                        // Número 1 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 50:                                        // Número 2 top
            case 98:                                        // Número 2 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 51:                                        // Número 3 top
            case 99:                                        // Número 3 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 52:                                        // Número 4 top
            case 100:                                       // Número 4 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 53:                                        // Número 5 top
            case 101:                                       // Número 5 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 54:                                        // Número 6 top
            case 102:                                       // Número 6 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 55:                                        // Número 7 top
            case 103:                                       // Número 7 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 56:                                        // Número 8 top
            case 104:                                       // Número 8 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 57:                                        // Número 9 top
            case 105:                                       // Número 9 left    
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 48:                                        // Número 0 top
            case 96:                                        // Número 0 left
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            // Operadores
            case 107:                                       // Sumar
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 109:                                       // Restar
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 106:                                       // Multiplicar
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
            case 111:                                       // Dividir
                addValue(valueKeyboard);                    // Envio el valor de la tecla al método
                break;
        }
    }
    // --
    addEventListener('keydown',e => {                       // Evento de teclado
        // console.log(e.keyCode);
        detectingkeyboard(e.keyCode,e.key);                 // Llamo función para solo poder pasar los números al programa.
    })

}